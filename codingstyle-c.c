/*
* Coding style C is a C program which provides infomation about my coding style.
*
* Copyright (C) <2016-2021>  <Peter Stevenson (2E0PGS)>
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

void help();

int main()
{
	char terminal_input[20];

	printf(
		"My Personal \"coding style\" guideline.\n\n"
		
		"To quote the Linux Kernel doc: \n"
		"\"Coding style is very personal, and I won't force my views on anybody.\"\n\n"

		"I also agree with this matter. I decided that I should layout a standard\n"
		"that I use for my programming. By setting a standardisation it benefits\n"
		"both me and any contrubuters which wish to add to my work.\n\n"

		"Type help for help information and command list.\n"
		"Please input request for more info on that topic: "
	);

	scanf("%s", terminal_input);

	printf("\n");

	if (strcmp(terminal_input, "csharp") == 0) {
		printf(
			"Variable names use camelCase.\n"
			"int exampleVariable;\n"
			"Braces are on new lines since visual studio prefers that.\n"
			"if (example == true)\n"
			"{\n"
			"    // Do something\n"
			"}\n"
			"Visual Studio also prefers spaces as opposed to tabs so that's what I use on C#.\n"
		);
	}
	else if (strcmp(terminal_input, "javascript") == 0) {
		printf(
			"camelCase.\n"
			"int exampleVariable;\n"
		);
	}
	else if (strcmp(terminal_input, "shell") == 0) {
		printf(
			"Variable names are all lowercase and use underscore to separate words.\n"
			"int example_variable;\n"
		);
	}
	else if (strcmp(terminal_input, "python") == 0) {
		printf(
			"Variable names are all lowercase and use underscore to separate words.\n"
			"int example_variable;\n"
			"See: https://www.python.org/dev/peps/pep-0008\n"
		);
	}
	else if (strcmp(terminal_input, "arduino") == 0) {
		printf(
			"Variable names use camelCase.\n"
			"int exampleVariable;\n"
		);
	}
	else if (strcmp(terminal_input, "braces") == 0) {
		printf(
			"The opening curly bracket is on same line as if statement and the closing on its own.\n"
			"if (x == true) {\n"
			"	// Do something\n"
			"}\n"
			"C# uses new line braces due to Visual Studio.\n"
		);
	}
	else if (strcmp(terminal_input, "IDE") == 0) {
		printf(
			"I use a tab width of 4 spaces.\n"
			"I use actual tabs. Apart from in languages that don't like tabs.\n"
			"I wrap around 80 - 120 ish columns text length.\n"
		);
	}
	else if (strcmp(terminal_input, "comments") == 0) {
		printf(
			"Comments are dependant on language. Most languages modern style comments are used.\n"
			"C language the older style comments are used.\n"
			"C# and C++ language the new style comments are used. Same goes for javascript etc.\n"
			"/* Old Style */\n"
			"// New style.\n"
			"Notice how the comment has a space before the text. This helps distinguish between commented out lines and comments.\n"
			"The first word of the comment should begin with a capital letter. Multi-Line comments begin with but new lines can be lowercase for first char.\n"
			"/*\n"
			"* Multi-Line comments look like this\n"
			"* foo\n"
			"* foo\n"
			"*/\n"
		);
	}
	else if (strcmp(terminal_input, "TLDR") == 0) {
		printf(
			"Too Long Didn't Read.\n"
			"Most languages use camelCase however shell and Python use snake_case, comment type is modern apart from C and CSS which use old comment style.\n"
			"C# has braces on new lines and uses spaces instead of tabs due to Visual Studio.\n"
		);
	}
	else if (strcmp(terminal_input, "help") == 0) {
		help();
	}
	else {
		printf("Command not recognised.\n");
	}

}/* End of main. */

void help()
{
	printf(
		"Command list:\n"
		"<command>  <description>\n"
		"csharp     -Prints Naming Convention I use for that language.\n"
		"javascript -Prints Naming Convention I use for that language.\n"
		"shell      -Prints Naming Convention I use for that language.\n"
		"python     -Prints Naming Convention I use for that language.\n"
		"arduino    -Prints Naming Convention I use for that language.\n"
		"braces     -Prints information about Braces and Brackets.\n"
		"IDE        -Prints information about my IDE setup.\n"
		"comments   -Prints information about commenting in code.\n"
		"TLDR       -Prints a summary of this document if your impatient and human.\n"
		"help       -Prints help screen. You are here.\n"
	);
}/* End of help. */
