# Coding style C

To quote the Linux Kernel doc: "Coding style is very personal, and I won't force my views on anybody".

I also agree with this matter. I decided that I should layout a standard that I use for my programming. By setting out some standardisation it benefits both me and any contributors who may wish to contribute to my project.

This is a C program which can output to terminal info on my coding style.

## Building

```sh
make
./codingstyle-c
```